sleep 7s
clear

# TEST ping 1
printf "\\n\\nTest %sping : It should respond pong:\\n" "$URL"
PONG=$(curl -s "$URL"ping -w "\\n" | jq -r '.message')
echo "$PONG" 
if [ "$PONG" = "pong" ]; then
	printf "Test passed."
else
	printf "Test not passed"
fi

sleep 2s

# TEST ping 10
NUMBER=10
printf "\\n\\nTest %scount : It should add %s to current count\\n" "$URL" "$NUMBER"
BASE=$(curl -s "$URL"count -w "\\n" | jq -r '.pingCount') 
printf "Before test  : %s\\n" "$BASE"

for i in `seq 1 $NUMBER`;
do 
	curl "$URL"'ping' -w "\\n" > /dev/null 2>&1	
done

AFTER=$(curl -s "$URL"count -w "\\n" | jq -r '.pingCount') 
printf "After test  : %s\\n" "$AFTER"

if [ $((AFTER - NUMBER)) = "$BASE" ]; then
	printf "Test passed.\\n"
else	
	printf "Test not passed\\n"
fi
