<p align="center">
  <img src ="https://cimg1.ibsrv.net/cimg/www.doityourself.com/660x300_100-1/785/ping-pong-paddles-183785.jpg" />
# Ping Pong Application

![Build Status](https://gitlab.com/guillaumeboutin66/PingPong-Step2/badges/master/build.svg)

# Installation
==========
* [Option 1 : Pull and Launch with Docker compose](https://gitlab.com/guillaumeboutin66/PingPong-Step2/#option-1-pull-and-launch-with-docker-compose)
* [Option 2 : Pull and Launch with Docker (Gitlab Artifact)](https://gitlab.com/guillaumeboutin66/PingPong-Step2/#option-2-pull-and-launch-with-docker-gitlab-artifact)
* [Option 3 : Pull and Launch Manually](https://gitlab.com/guillaumeboutin66/PingPong-Step2/#option-3-pull-and-launch-manually)

## Option 1 : Pull and Launch with Docker compose
### Pull
    
    git clone https://gitlab.com/guillaumeboutin66/PingPong-Step2.git 

    or

    download zip

### Docker compose
    
    #go in contener-compose folder
    docker-compose up

==========

## Option 2 : Pull and Launch with Docker (Gitlab Artifact)
First log in to GitLab’s Container Registry using your GitLab username and password. If you have 2FA enabled you need to use a personal access token:

    docker login registry.gitlab.com

### Pull

    docker pull registry.gitlab.com/guillaumeboutin66/pingpong-step2/pingcount/server:master
    docker pull registry.gitlab.com/guillaumeboutin66/pingpong-step2/pingcount/test:master

### Run (Automatically pull)

    docker run registry.gitlab.com/guillaumeboutin66/pingpong-step2/pingcount/server:master
    docker run registry.gitlab.com/guillaumeboutin66/pingpong-step2/pingcount/test:master

==========

## Option 3 : Pull and Launch Manually
### Pull
    
    git clone https://gitlab.com/guillaumeboutin66/PingPong-Step2.git 

    or

    download zip

Need Node.js installed :

    apt-get install nodejs npm
    apt-get update nodejs npm

### Install

Need Raven (*for Sentry*) installed :

    npm install raven --save
    
### Launch

    node server.js
    
### Test

To check if the server is launched, just execute checkApi.sh script :
  -  On linux :
  
    ./checkApi.sh
    
      
  -  On MacOs
  
    sh ./checkApi.sh

## Infos

You can easily change variables to adapt to the workspace : 
  -  var fileCount = le nom du fichier contenant les count
  -  var port = le port
  -  key raven, change Raven.config('https://***:***@sentry.io/257042').install();
  

And on the test file (testApi.sh), 
you need to use the same PORT present in server.js
in urls variables (URLPING, URLCOUNT)

