# Groupe 2 - Ping Pong
==================


Need Node.js installed :

    apt-get install nodejs npm
    apt-get update nodejs npm
    
    

Launch with :

    node server.js



You can easily change variables to adapt to the workspace : 
  -  var fileCount = le nom du fichier contenant les count
  -  var port = le port
  -  key raven, change Raven.config('https://***:***@sentry.io/257042').install();
 
 

And on the test file (testApi.sh), 
you need to use the same PORT present in server.js
in urls variables (URLPING, URLCOUNT)



To check if the server is launched, just execute checkApi.sh script :
  -  On linux :
  
    ./checkApi.sh
    
      
  -  On MacOs
  
    sh ./checkApi.sh
     