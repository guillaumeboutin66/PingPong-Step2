// Need: Node.JS
// RUN: node server.js

// import native
var http = require('http');
var url = require('url');
var path = require('path');
var fs = require('fs');

// Import Raven for Sentry // npm install raven --save
//var Raven = require('raven');
//Raven.config('https://6d962cbc7c844dbba98879a5b2887161:9b54f81bab084e108ae327eb3ca2bbd9@sentry.io/257042').install();

// Port
var port = 3466;

// File
var fileCount = 'fileCountPing.txt';
var counter = 0;

// Create the server
http.createServer(function (request, response) {

    // The requested URL
    var uri = url.parse(request.url).pathname;

    // The requested URL is PING -- Update file ++
    if(uri == "/ping"){
        response.writeHead(200, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify({ message: 'pong' }));
        var callback = function(){
            countFile(1)
        }
        testExist(callback);
    }
    // The requested URL is COUNT -- Return value in file
    else if(uri == "/count"){
        response.writeHead(200, { 'Content-Type': 'application/json' });
        var callback = function(){
            countFile(0)
            response.end(JSON.stringify({ pingCount: counter }));
        }
        testExist(callback);
    }
    // The requested URL is RESET -- Reset file
    else if(uri == "/reset"){
        response.writeHead(200, { 'Content-Type': 'application/json' });
        var callback = function(){
            countFile(-1 * counter)
            response.end(JSON.stringify({ pingCount: 0 }));
        }
        testExist(callback);
    }
    // The requested is not implemented
    else{
        response.writeHead(501, { 'Content-Type': 'application/json' });
        response.end(JSON.stringify({ message: 'Not implemented' }));
    }

    function testExist(callback) {
        // Check if the requested file exists
        fs.exists(fileCount, function (exists) {
            // If it doesn't
            if (!exists) {
                //Raven.captureException('fileCountPing.txt Not exist');
                // Create file
                try{
                    fs.writeFileSync(fileCount, "0", 'utf8')
                    callback();
                }
                // If err, Capture exception why not CREATE + response 500
                catch(err){
                    //Raven.captureException('Could not CREATE the file : ' + err.message);
                    response.writeHead(500, { 'Content-Type': 'application/json' });
                    response.end(JSON.stringify({ message: 'Could not CREATE the file : ' + err.message }));
                }          
            }else{
                callback();
            }        
        });
    }

    function countFile(update) {
        fs.readFile(fileCount, function read(err, data) {
            // If err, Capture exception  why not UPDATE + response 500
            if (err) {
                //Raven.captureException("Could not READ the file : " + err.message);
                response.writeHead(500, { 'Content-Type': 'application/json' });
                response.end(JSON.stringify({ message: 'Could not READ the file : ' + err.message }));
            }
            // Update file
            try{
                counter = parseInt(data, 10)+update
                fs.writeFileSync(fileCount, parseInt(data, 10)+update, 'utf8');
            }
            // If err, Capture exception why not UPDATE + response 500
            catch(err){
                //Raven.captureException('Could not UPDATE the file' + err.message);
                response.writeHead(500, { 'Content-Type': 'application/json' });
                response.end(JSON.stringify({ message: 'Could not UPDATE the file' + err.message }));
            }
        });
    }

}).listen(parseInt(port, 10));

// Message to display when server is started
//Raven.captureException('Server running at\n  => http://localhost:' + port);